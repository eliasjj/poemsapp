﻿using Prism.Commands;
using Prism.Mvvm;
using Prism.Navigation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BlankApp2.ViewModels
{
    public class MainPageViewModel : ViewModelBase
    {
        private INavigationService _navigationService;
        public DelegateCommand<object> ChoosePoem { get; private set; }

        public MainPageViewModel(INavigationService navigationService) : base (navigationService)
        {
            Title = "Main Page";
            _navigationService = navigationService;
                
            // команда касания(перехода на страницу считалочки с главного экрана)
            ChoosePoem = new DelegateCommand<object>(OnTapped);
        }

        private void OnTapped(object args)
        {
            _navigationService.NavigateAsync(args.ToString());

        }
    }
}
