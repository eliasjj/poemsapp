﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using Prism.Navigation;
using Prism.Services;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Reflection;
using System.IO;
using Plugin.SimpleAudioPlayer;

namespace BlankApp2.ViewModels
{
	public class OrangePoemPageViewModel : BindableBase
	{
        private INavigationService _navigationService;
        private ISimpleAudioPlayer SAPlayer;
        public DelegateCommand SlideShow { get; private set; }
        public DelegateCommand GoHome { get; private set; }
        private string _backImgSource = "orange_1.png";
        private string _backImgSource2 = "orange_2.png";
        private string _goImgSource = "go.png";
        private string _homeImgSource = "home.png";
        private bool _visibleHome = false;
        private bool _visibleSlideShow = true;
        private int _opacity = 1;
        private int _opacity1 = 0;

        public int Opacity
        {
            get { return _opacity; }
            set { SetProperty(ref _opacity, value); }
        }

        public int Opacity1
        {

            get { return _opacity1; }
            set { SetProperty(ref _opacity1, value); }
        }

        public string BackImgSource
        {
            get { return _backImgSource; }
            set { SetProperty(ref _backImgSource, value); }
        }

        public string BackImgSource2
        {
            get { return _backImgSource2; }
            set { SetProperty(ref _backImgSource2, value); }
        }

        public bool VisibleHomeBtn
        {
            get { return _visibleHome; }
            set { SetProperty(ref _visibleHome, value); }
        }

        public bool VisibleSlideShowBtn
        {
            get { return _visibleSlideShow; }
            set { SetProperty(ref _visibleSlideShow, value); }
        }

        public string GoImgSource
        {
            get { return _goImgSource; }
            set { SetProperty(ref _goImgSource, value); }
        }

        public string HomeImgSource
        {
            get { return _homeImgSource; }
            set { SetProperty(ref _homeImgSource, value); }
        }

        public OrangePoemPageViewModel(INavigationService navigationService)
        {
            SlideShow = new DelegateCommand(MakeShow);
            GoHome = new DelegateCommand(Home);

            _navigationService = navigationService;

        }

        private async void MakeShow()
        {
            VisibleHomeBtn = true;
            VisibleSlideShowBtn = false;
            string[] images = { "orange_2.png", "orange_3.png", "orange_4.png", "orange_5.png",
                "orange_6.png", "orange_7.png", "orange_8.png", "orange_9.png",
                "orange_10.png", "orange_11.png", "orange_12.png"};
            
            // audio player
            SAPlayer = CrossSimpleAudioPlayer.CreateSimpleAudioPlayer();
            SAPlayer.Load(GetStreamFromFile($"Sounds.orange.wav"));
            SAPlayer.Play();
            //

            // смена картинок
            Opacity = 0;
            Opacity1 = 1;
            BackImgSource = "orange_3.png";
            await Task.Delay(3500);
            Opacity = 1;
            Opacity1 = 0;
            BackImgSource2 = "orange_4.png";
            await Task.Delay(1500);
            Opacity = 0;
            Opacity1 = 1;
            BackImgSource = "orange_5.png";
            await Task.Delay(1500);
            Opacity = 1;
            Opacity1 = 0;
            BackImgSource2 = "orange_6.png";
            await Task.Delay(2500);
            Opacity = 0;
            Opacity1 = 1;
            BackImgSource = "orange_7.png";
            await Task.Delay(2500);
            Opacity = 1;
            Opacity1 = 0;
            BackImgSource2 = "orange_8.png";
            await Task.Delay(2500);
            Opacity = 0;
            Opacity1 = 1;
            BackImgSource = "orange_9.png";
            await Task.Delay(2500);
            Opacity = 1;
            Opacity1 = 0;
            BackImgSource2 = "orange_10.png";
            await Task.Delay(2500);
            Opacity = 0;
            Opacity1 = 1;
            BackImgSource = "orange_11.png";
            await Task.Delay(2500);
            Opacity = 1;
            Opacity1 = 0;
            BackImgSource2 = "orange_12.png";
            await Task.Delay(2500);
            Opacity = 0;
            Opacity1 = 1;
            //
        }

        Stream GetStreamFromFile(string filename)
        {
            var assembly = typeof(App).GetTypeInfo().Assembly;
            var stream = assembly.GetManifestResourceStream("BlankApp2." + filename);

            return stream;
        }

        private void Home()
        {
            _navigationService.GoBackAsync();
            SAPlayer.Stop();
        }
    }
}
