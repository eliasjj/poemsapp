﻿using Prism.Commands;
using Prism.Mvvm;
using System;
using System.Collections.Generic;
using System.Linq;
using Prism.Navigation;
using Prism.Services;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Reflection;
using System.IO;
using Plugin.SimpleAudioPlayer;



namespace BlankApp2.ViewModels
{
    public class HidePoemPageViewModel : BindableBase
    {
        private INavigationService _navigationService;
        private ISimpleAudioPlayer SAPlayer;
        public DelegateCommand SlideShow { get; private set; }
        public DelegateCommand GoHome { get; private set; }
        private string _backImgSource = "hide_and_go_seek_1.png";
        private string _backImgSource2 = "hide_and_go_seek_2.png";
        private string _goImgSource = "go.png";
        private string _homeImgSource = "home.png";
        private bool _visibleHome = false;
        private bool _visibleSlideShow = true;
        private int _opacity = 1;
        private int _opacity1 = 0;

        public int Opacity
        {
            get { return _opacity; }
            set { SetProperty(ref _opacity, value); }
        }

        public int Opacity1
        {

            get { return _opacity1; }
            set { SetProperty(ref _opacity1, value); }
        }

        public string BackImgSource
        {
            get { return _backImgSource; }
            set { SetProperty(ref _backImgSource, value); }
        }

        public string BackImgSource2
        {
            get { return _backImgSource2; }
            set { SetProperty(ref _backImgSource2, value); }
        }

        public string GoImgSource
        {
            get { return _goImgSource; }
            set { SetProperty(ref _goImgSource, value); }
        }

        public string HomeImgSource
        {
            get { return _homeImgSource; }
            set { SetProperty(ref _homeImgSource, value); }
        }

        public bool VisibleHomeBtn
        {
            get { return _visibleHome; }
            set { SetProperty(ref _visibleHome, value); }
        }

        public bool VisibleSlideShowBtn
        {
            get { return _visibleSlideShow; }
            set { SetProperty(ref _visibleSlideShow, value); }
        }

        public HidePoemPageViewModel(INavigationService navigationService)
        {
            SlideShow = new DelegateCommand(MakeShow);
            GoHome = new DelegateCommand(Home);

            _navigationService = navigationService;

        }

        private async void MakeShow()
        {
            GoImgSource = "go_pressed.png";
            VisibleHomeBtn = true;
            VisibleSlideShowBtn = false;
            
            // audio player
            SAPlayer = CrossSimpleAudioPlayer.CreateSimpleAudioPlayer();
            SAPlayer.Load(GetStreamFromFile($"Sounds.hide.wav"));
            SAPlayer.Play();
            //

            // смена картинок
            Opacity = 0;
            Opacity1 = 1;
            BackImgSource = "hide_and_go_seek_3.png";
            await Task.Delay(1500);
            Opacity = 1;
            Opacity1 = 0;
            BackImgSource2 = "hide_and_go_seek_4.png";
            await Task.Delay(1000);
            Opacity = 0;
            Opacity1 = 1;
            BackImgSource = "hide_and_go_seek_5.png";
            await Task.Delay(1000);
            Opacity = 1;
            Opacity1 = 0;
            BackImgSource2 = "hide_and_go_seek_6.png";
            await Task.Delay(1000);
            Opacity = 0;
            Opacity1 = 1;
            BackImgSource = "hide_and_go_seek_7.png";
            await Task.Delay(1800);
            Opacity = 1;
            Opacity1 = 0;
            BackImgSource2 = "hide_and_go_seek_8.png";
            await Task.Delay(2000);
            Opacity = 0;
            Opacity1 = 1;
            BackImgSource = "hide_and_go_seek_9.png";
            await Task.Delay(1000);
            Opacity = 1;
            Opacity1 = 0;
            BackImgSource2 = "hide_and_go_seek_10.png";
            await Task.Delay(1000);
            Opacity = 0;
            Opacity1 = 1;
            BackImgSource = "hide_and_go_seek_11.png";
            await Task.Delay(1000);
            Opacity = 1;
            Opacity1 = 0;
            BackImgSource2 = "hide_and_go_seek_12.png";
            await Task.Delay(1000);
            Opacity = 0;
            Opacity1 = 1;
            //
        }

        Stream GetStreamFromFile(string filename)
        {
            var assembly = typeof(App).GetTypeInfo().Assembly;
            var stream = assembly.GetManifestResourceStream("BlankApp2." + filename);

            return stream;
        }

        private void Home()
        {
            HomeImgSource = "home_pressed.png";
            _navigationService.GoBackAsync();
            SAPlayer.Stop();
        }


    }
}
